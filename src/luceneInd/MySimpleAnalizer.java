package luceneInd;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.CharArraySet;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * Created by Sandro on 3/25/16.
 * Simple analyzer: stopword
 */
public class MySimpleAnalizer extends Analyzer {
    CharArraySet STOPWORDS;

    public MySimpleAnalizer(CharArraySet STOPWORDS) {
        this.STOPWORDS = STOPWORDS;
    }

    @Override
    protected TokenStreamComponents createComponents(String s) {
        Reader reader = new StringReader(s);
        Tokenizer source = new StandardTokenizer();
        try {
            source.setReader(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //stopword removal and porterStemming
        return new TokenStreamComponents(source, new StopFilter(new StandardFilter(source), STOPWORDS));
    }
}
