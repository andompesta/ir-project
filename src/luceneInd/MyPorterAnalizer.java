package luceneInd;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseTokenizer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.util.CharArraySet;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * Created by Sandro on 15/3/16.
 * Porter analyzer: lowercase + stopword + porter stemming
 */
public class MyPorterAnalizer extends Analyzer {

    CharArraySet STOPWORDS;

    public MyPorterAnalizer(CharArraySet STOPWORDS) {
        this.STOPWORDS = STOPWORDS;
    }

    @Override
    protected TokenStreamComponents createComponents(String s) {
        Reader reader = new StringReader(s);
        //lowercase
        Tokenizer source = new LowerCaseTokenizer();
        try {
            source.setReader(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //stopword removal and porterStemming
        return new TokenStreamComponents(source, new PorterStemFilter(new StopFilter(source, STOPWORDS)));
    }
}
