package luceneInd;


import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import parser.PaperInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;



/**
 * Created by Sandro on 15/3/16.
 * Class used to create a lucene index. It specify how the fields are indexed
 */
public class IndexingFiles {
    //directory where save the index
    private String INDEX_PATH;
    //remove the lucene stopword
    private CharArraySet STOPWORD;
    private IndexWriter indexWriter;


    public IndexingFiles(String path_index) throws IOException {
        //read the path
        this.INDEX_PATH = path_index;
        File indexDir = new File(INDEX_PATH + File.separator + "index");

        if (! indexDir.exists()){
            indexDir.mkdir();
        }else{
            FileUtils.cleanDirectory(indexDir);
        }
    }


    public void openWriter(boolean updateExisting, boolean useStopword, boolean usePorterStemmer, boolean useLowercaseNormalizatio) throws IOException {
        System.out.println("Indexing to directory '" + INDEX_PATH + File.separator + "index");
        Directory articleIndexDir = FSDirectory.open(Paths.get(INDEX_PATH+ File.separator + "index"));

        if (useStopword){
            System.out.println("indexing using stopwords");
            try (BufferedReader br = new BufferedReader(new FileReader("dataset/stopword.list")))
            {
                String sCurrentLine;
                Set<String> temp = new HashSet<>();
                while ((sCurrentLine = br.readLine()) != null) {
                    temp.add(sCurrentLine.trim());
                }

                this.STOPWORD = CharArraySet.unmodifiableSet(CharArraySet.copy(temp));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            System.out.println("indexing without stopwords");
            this.STOPWORD = CharArraySet.EMPTY_SET;
        }

        Analyzer analyzer = null;
        if (usePorterStemmer){
            System.out.println("indexing with stemming");
            analyzer = new MyPorterAnalizer(this.STOPWORD);
        } else if (useLowercaseNormalizatio){
            System.out.println("indexing with lowercase");
            // creates tokens using the Word Break rules from the Unicode Text Segmentation algorithm specified in Unicode Standard Annex #29;
            // converts tokens to lowercase;
            // and then filters out stopwords
            analyzer = new StandardAnalyzer(this.STOPWORD);
        } else if (!useLowercaseNormalizatio){
            System.out.println("indexing with no lowercase normalization");
            analyzer = new MySimpleAnalizer(this.STOPWORD);
        }

        Map<String,Analyzer> analyzerPerField = new HashMap<String,Analyzer>();
        analyzerPerField.put(SearchIndex.myFields.title.name(), analyzer);
        PerFieldAnalyzerWrapper wrapper = new PerFieldAnalyzerWrapper(new StandardAnalyzer(), analyzerPerField);

        IndexWriterConfig indexIwc = new IndexWriterConfig(wrapper);

        if (!updateExisting) {
            indexIwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        } else {
            // Add new documents to an existing index:
            indexIwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        }

        indexWriter = new IndexWriter(articleIndexDir, indexIwc);
    }

    public void closeWriter() throws IOException {
        indexWriter.close();
    }


    //start to index the documents
	public void indexDocument( Collection<PaperInfo> paperInfos) throws Exception {
		Date start = new Date();
		try {
            if (paperInfos != null)
                indexDocs(indexWriter, paperInfos);
            else
                throw new Exception("");

			Date end = new Date();
			System.out.println("Time " + (end.getTime() - start.getTime()) + " total milliseconds");
			
		} catch (IOException e) {
			System.out.println(" caught a " + e.getClass() +
			"\n with message: " + e.getMessage());
        }
	}

    //iterate trought all the pubblication
    private void indexDocs(final IndexWriter indexWriter, Collection<PaperInfo> paperInfos) throws IOException {
        for (Iterator<PaperInfo> infoIterator = paperInfos.iterator(); infoIterator.hasNext();){
            PaperInfo paperInfo = infoIterator.next();
            indexDoc(indexWriter, paperInfo);
        }
    }


	
    //indexer
	private void indexDoc(IndexWriter indexWriter, PaperInfo paperInfo) throws IOException {
        Document doc = new Document();
        FieldType titleField = new FieldType();
        titleField.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        titleField.setTokenized(true);
        titleField.setStored(true);
        titleField.setStoreTermVectors(true);

        /*
        String authors = "";
        for (String author : paperInfo.getAuthors()){
            authors += author + ", ";
        }
        if (authors.length() > 2){
            authors = authors.substring(0, authors.length() - 2);
        }

        //indexed and tokenized and stored
        Field text_field = new TextField(SearchIndex.myFields.authors.name(), authors, Field.Store.YES);
        doc.add(text_field);
        */

        for (String author : paperInfo.getAuthors()){
            Field text_field = new TextField(SearchIndex.myFields.authors.name(), author, Field.Store.YES);
            doc.add(text_field);
        }


        Field paper_id_field = new TextField(SearchIndex.myFields.paper_id.name(), paperInfo.getKey(), Field.Store.YES );
        doc.add(paper_id_field);

        if (paperInfo.getTitle() == null){
            System.err.println(paperInfo.toString());
        } else {
            //indexed, tokenized amd stored
            Field title_field = new Field(SearchIndex.myFields.title.name(), paperInfo.getTitle(), titleField);
            doc.add(title_field);
        }

        Field year_field = new IntField(SearchIndex.myFields.year.name(), paperInfo.getYear(), Field.Store.YES);
        doc.add(year_field);

        Field venue_field = new TextField(SearchIndex.myFields.venue.name(), paperInfo.getVenue(), Field.Store.YES );
        doc.add(venue_field);


        Field type_field = new StringField(SearchIndex.myFields.type.name(), paperInfo.getType(), Field.Store.YES);
        doc.add(type_field);

        if (indexWriter.getConfig().getOpenMode() == IndexWriterConfig.OpenMode.CREATE) {
            indexWriter.addDocument(doc);
        } else {
            //System.out.println("updating " + file);
            indexWriter.updateDocument(new Term("paper_id", paperInfo.getKey()), doc);
        }
	}
}
