package luceneInd;

import DAO.YearVocabularyAndDocs;
import similarity.Venue;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualLinkedHashBidiMap;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import parser.PaperInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Sandro on 15/3/16.
 * HelperClass that execute the query on the luceneindexes
 */
public class SearchIndex {
    private String INDEX_PATH;
    private CharArraySet STOPWORD;

    private DirectoryReader indexDir;
    private IndexSearcher indexSearcher;
    private boolean indexOpen;
    private PerFieldAnalyzerWrapper wrapper;

    public enum myFields {authors, paper_id, title, year, venue, type, any, luceneQuery};


    public SearchIndex(String path, boolean useStopword, boolean usePorterStemmer, boolean useLowercaseNormalizatio) {
        INDEX_PATH = path;

        Analyzer analyzer = null;
        if (useStopword){
            try (BufferedReader br = new BufferedReader(new FileReader(INDEX_PATH + File.separator + "stopword.list")))
            {
                String sCurrentLine;

                Set<String> temp = new HashSet<>();
                while ((sCurrentLine = br.readLine()) != null) {
                    temp.add(sCurrentLine.trim());
                }
                this.STOPWORD = CharArraySet.unmodifiableSet(CharArraySet.copy(temp));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            this.STOPWORD = CharArraySet.EMPTY_SET;
        }

        if (usePorterStemmer){
            analyzer = new MyPorterAnalizer(this.STOPWORD);
        }else if (useLowercaseNormalizatio){
            //creates tokens using the Word Break rules from the Unicode Text Segmentation algorithm specified in Unicode Standard Annex #29;
            // converts tokens to lowercase;
            // and then filters out stopwords
            analyzer = new StandardAnalyzer(this.STOPWORD);
        } else if (!useLowercaseNormalizatio){
            analyzer = new MySimpleAnalizer(this.STOPWORD);
        }

        Map<String,Analyzer> analyzerPerField = new HashMap<String,Analyzer>();
        analyzerPerField.put(SearchIndex.myFields.title.name(), analyzer);
        wrapper = new PerFieldAnalyzerWrapper(new StandardAnalyzer(), analyzerPerField);

    }


    public void openIndex() throws IOException{
        indexDir = DirectoryReader.open(FSDirectory.open(Paths.get(INDEX_PATH+ File.separator + "index")));
        indexSearcher = new IndexSearcher(indexDir);
        indexOpen = true;
        System.err.println("open index");
    }

    public void closeIndex() throws IOException{
        indexDir.close();
        indexOpen = false;
        System.err.println("closed inproceedings index \n\n\n");
    }


    //Execute the search for a query coming form the UI
    public boolean doSearch(Map<myFields, Object> fields, int topN, boolean rawOutputFormat, boolean interactive, BufferedReader in) throws IOException, ParseException {
        boolean ret = false;
        IndexSearcher searcher = null;
        this.openIndex();
        searcher = this.indexSearcher;



        BooleanQuery booleanQuery = new BooleanQuery();
        for (myFields aField : fields.keySet()){
            String queryValue = fields.get(aField).toString();
            switch (aField){
                case authors: case paper_id: case title: case venue: case type:
                    Query q  = processStringQuery(queryValue, aField);
                    booleanQuery.add(q, BooleanClause.Occur.MUST);
                    break;
                case year:
                    int yearVale = Integer.parseInt(queryValue);
                    booleanQuery.add( NumericRangeQuery.newIntRange(aField.name(), yearVale, yearVale, true, true ), BooleanClause.Occur.MUST);
                    break;
                case any:
                    for (myFields field : new myFields[]{myFields.authors, myFields.paper_id, myFields.title, myFields.venue, myFields.type}){
                        booleanQuery.add(processStringQuery(queryValue, field), BooleanClause.Occur.SHOULD);
                    }
                    try{
                        int yearVale1 = Integer.parseInt(queryValue);
                        booleanQuery.add( NumericRangeQuery.newIntRange(myFields.year.name(), yearVale1, yearVale1, true, true ), BooleanClause.Occur.MUST);
                    } catch (NumberFormatException nfe){
                        nfe.printStackTrace();
                    }
                    break;

            }
        }

        System.out.println("Execute query: " + booleanQuery.toString());
        ret = doPagingSearch(in, searcher, booleanQuery, topN, rawOutputFormat, interactive );

        if (indexOpen)
            closeIndex();
        return ret;
    }

    //execute the query with pagination for the UI interaction
    private boolean doPagingSearch(BufferedReader in, IndexSearcher searcher, Query query, int hitsPerPage, boolean raw, boolean interactive) throws IOException, ParseException {
        // Collect enough docs to show 5 pages
        TopDocs results = searcher.search(query, 5 * hitsPerPage);
        ScoreDoc[] hits = results.scoreDocs;
        int numTotalHits = results.totalHits;
        System.out.println(numTotalHits + " total matching documents");
        int start = 0;
        int end = Math.min(numTotalHits, hitsPerPage);


        while (true) {
            if (end > hits.length) {
                System.out.println("Only results - " + hits.length + " of " + numTotalHits + " total matching documents collected.");
                System.out.println("Collect more (y/n) ?");
                String inputLine = in.readLine();
                if (inputLine.length() == 0 || inputLine.charAt(0) == 'n') {
                    return false;
                }
                hits = searcher.search(query, numTotalHits).scoreDocs;
            }
            end = Math.min(hits.length, start + hitsPerPage);

            for (int i = start; i < end; i++) {
                if (raw) {                              // output raw format
                    System.out.println("doc=" + hits[i].doc + " score=" + hits[i].score);
                    continue;
                }
                Document doc = searcher.doc(hits[i].doc);
                printDoc(doc, i, hits[i].score, hits[i].doc);

            }
            if (!interactive || end == 0) {
                return false;
            }

            if (numTotalHits >= end) {
                while (true) {
                    System.out.print("Press ");
                    if (start - hitsPerPage >= 0) {
                        System.out.print("(p)revious page, ");
                    }
                    if (start + hitsPerPage < numTotalHits) {
                        System.out.print("(n)ext page, ");
                    }
                    System.out.println("(N)ew query, (q)uit  or enter number to jump to a page.");
                    String line = in.readLine();
                    //exit condition, close the program
                    if (line.length() == 0 || line.charAt(0) == 'q') {
                        return true;
                    }
                    //Enter a new wuery
                    if (line.charAt(0) == 'N') {
                        return false;
                    }
                    //go to the next page if any
                    if (line.charAt(0) == 'p') {
                        start = Math.max(0, start - hitsPerPage);
                        break;
                    }
                    //go to the previous page if any
                    else if (line.charAt(0) == 'n') {
                        if (start + hitsPerPage < numTotalHits) {
                            start += hitsPerPage;
                        }
                        break;
                    }
                    //go to a specific page
                    else {
                        try {
                            int page = Integer.parseInt(line);
                            if ((page - 1) * hitsPerPage < numTotalHits) {
                                start = (page - 1) * hitsPerPage;
                                break;
                            } else {
                                System.out.println("No such page");
                            }
                        } catch (NumberFormatException e) {
                            //in case of error input stop
                            break;
                        }
                    }
                }
                end = Math.min(numTotalHits, start + hitsPerPage);
            }
        }
    }

    //Execute the search for a query coming form the evaluation
    public List<PaperInfo> doSearch(Map<myFields, Object> fields) throws IOException, ParseException {
        IndexSearcher searcher = null;
        this.openIndex();
        searcher = this.indexSearcher;



        BooleanQuery booleanQuery = new BooleanQuery();
        for (myFields aField : fields.keySet()){
            switch (aField){
                case authors: case paper_id: case title: case venue:case type:
                    String queryValue = fields.get(aField).toString();
                    booleanQuery.add(processStringQuery(queryValue, aField), BooleanClause.Occur.MUST);
                    break;
                case year:
                    int yearVale = Integer.parseInt(fields.get(aField).toString().replace(".0",""));
                    booleanQuery.add( NumericRangeQuery.newIntRange(aField.name(), yearVale, yearVale, true, true ), BooleanClause.Occur.MUST);
                    break;
            }
        }

        System.out.println("Execute query: " + booleanQuery.toString());
        fields.put(myFields.luceneQuery, booleanQuery.toString());

        List<PaperInfo> ret = executeQuery(searcher, booleanQuery, 10);

        if (indexOpen)
            closeIndex();
        return ret;
    }
    //execute the evaluation query
    private List<PaperInfo> executeQuery(IndexSearcher searcher, BooleanQuery booleanQuery, int hitsPerPage) throws IOException {
        // Collect enough docs to show 5 pages
        TopDocs results = searcher.search(booleanQuery, hitsPerPage);

        List<PaperInfo> ret = new ArrayList<>();

        ScoreDoc[] hits = results.scoreDocs;
        int numTotalHits = results.totalHits;
        System.out.println(numTotalHits + " total matching documents");


        for (int i = 0; i < hits.length; i++){
            Document doc = searcher.doc(hits[i].doc);
            List<String> authors = new ArrayList<>();
            for (IndexableField authorField : doc.getFields(myFields.authors.name())) {
                authors.add(authorField.stringValue());
            }

            PaperInfo pf = new PaperInfo(doc.get(myFields.paper_id.name()), doc.get(myFields.title.name()), authors,
                    Integer.parseInt(doc.get(myFields.year.name())), doc.get(myFields.venue.name()), doc.get(myFields.type.name()) );
            pf.score = hits[i].score;
            ret.add(pf);
        }

        return ret;
    }



    //get vocabulary dimensions
    public int getVocabulary(myFields aField) throws Exception {
        openIndex();
        Set<String> ret = new HashSet<>();
        try{
            Fields fields = MultiFields.getFields(indexSearcher.getIndexReader());
            Terms terms = fields.terms(aField.name());
            TermsEnum iterator = terms.iterator(null);
            BytesRef byteRef = null;
            while((byteRef = iterator.next()) != null) {
                String term = new String(byteRef.bytes, byteRef.offset, byteRef.length);
                ret.add(term);
            }

        }catch (Exception ex){
            throw ex;
        }finally {
            if (indexOpen)
                closeIndex();
        }
        return ret.size();
    }

    //get all paper's titles for a given years
    public List<YearVocabularyAndDocs> getVocabularyDocument(myFields aField, int startYear, int endYear) throws IOException {
        openIndex();
        List<YearVocabularyAndDocs> ret = new ArrayList<>();
        try{
            for (int year = startYear; year <= endYear; year++){
                YearVocabularyAndDocs yearData = new YearVocabularyAndDocs();
                yearData.setYear(year);
                Query yearQuery = NumericRangeQuery.newIntRange(myFields.year.name(), year, year, true, true );

                TotalHitCountCollector collector = new TotalHitCountCollector();
                indexSearcher.search(yearQuery, collector);
                TopDocs results = indexSearcher.search(yearQuery, Math.max(1, collector.getTotalHits()));
                ScoreDoc[] hits = results.scoreDocs;
                System.out.println("total documents for year " + year + " " + hits.length);

                BidiMap<String, Integer> yearVocabulary = new DualLinkedHashBidiMap<>();
                List<DAO.Document> yearDocs = new ArrayList<>();

                for (int i = 0; i < Math.min(hits.length, 1000000000); i++){
                    //Document path
                    Document doc = indexSearcher.doc(hits[i].doc);
                    Terms doc_terms = indexSearcher.getIndexReader().getTermVector(hits[i].doc, aField.name());
                    if (doc_terms != null){

                        List<Integer> worList = new ArrayList<>();
                        String content = "";
                        TermsEnum it  = null;
                        it = doc_terms.iterator(it);
                        BytesRef text = null;
                        while ((text = it.next()) != null) {
                            String term = text.utf8ToString();
                            if (!yearVocabulary.containsKey(term))
                                yearVocabulary.put(term, yearVocabulary.size());

                            worList.add(yearVocabulary.get(term));
                            content += term + " ";
                        }
                        yearDocs.add(new DAO.Document(worList, hits[i].doc, content) );
                    }
                    /*
                    else{
                        System.out.println("------------ doc with no title ------------");
                        printDoc(doc, i, hits[i].score, hits[i].doc);
                    }
                    */
                }

                yearData.setYearsVocabulary(yearVocabulary);
                yearData.setYearDocuments(yearDocs);
                ret.add(yearData);
            }
        }catch (Exception ex){
            throw ex;
        }finally {
            if (indexOpen)
                closeIndex();
        }
        return ret;
    }


    public List<String> getPaperTitles(Venue venue) throws Exception {
        List<String> ret = new ArrayList<>();

        BooleanQuery booleanClauses = new BooleanQuery();
        QueryParser parser = new QueryParser(myFields.venue.name(), wrapper);
        Query q  = parser.parse("\"" + venue.venueName + "\"");

        Query year_q = NumericRangeQuery.newIntRange(myFields.year.name(), venue.year, venue.year, true, true );
        booleanClauses.add(q, BooleanClause.Occur.MUST);
        booleanClauses.add(year_q, BooleanClause.Occur.MUST);


        TotalHitCountCollector collector = new TotalHitCountCollector();
        indexSearcher.search(q, collector);
        TopDocs results = indexSearcher.search(q, Math.max(1, collector.getTotalHits()));
        ScoreDoc[] hits = results.scoreDocs;

        //System.out.println("total documents for venue " + venue.venueName + "/" + venue.year + "\t" + hits.length);

        for (int i = 0; i < hits.length; i++){
            //Document path
            Document doc = indexSearcher.doc(hits[i].doc);
            String title = doc.get(myFields.title.name());
            String venueName = doc.get(myFields.venue.name());
            int year = Integer.parseInt(doc.get(myFields.year.name()));
            if(venue.year == year && venue.venueName.toLowerCase().equals(venueName.toLowerCase())){
                title = removeRepated(title.toLowerCase());
                title = removeNumbers(title);
                ret.add(title);
            }
        }
        return ret;
    }


    public Collection<Venue> getPaperVenue(String title) throws Exception {
        Set<Venue> ret = new HashSet<>();

        String queryValue = title;
        if ( queryValue.startsWith("\"") && queryValue.endsWith("\"") ){
            queryValue = queryValue.substring(1, queryValue.length() - 1);
            queryValue = QueryParser.escape(queryValue);
            queryValue = "\"" + queryValue + "\"";
        } else{
            queryValue = QueryParser.escape(queryValue);
        }

        QueryParser parser = new QueryParser(myFields.title.name(), wrapper);
        Query q  = parser.parse(queryValue);


        TotalHitCountCollector collector = new TotalHitCountCollector();
        indexSearcher.search(q, collector);
        TopDocs results = indexSearcher.search(q, Math.max(1, Math.min(5, collector.getTotalHits()) ) );
        ScoreDoc[] hits = results.scoreDocs;

        for (int i = 0; i < hits.length; i++){
            Document doc = indexSearcher.doc(hits[i].doc);
            String venue = doc.get(myFields.venue.name());
            int year = Integer.parseInt(doc.get(myFields.year.name()));
            ret.add(new Venue(venue, year));
        }
        return ret;
    }

    private Query processStringQuery(String queryValue, myFields aField) throws ParseException {
        if ( queryValue.startsWith("\"") && queryValue.endsWith("\"") ){
            queryValue = queryValue.substring(1, queryValue.length() - 1);
            queryValue = QueryParser.escape(queryValue);
            queryValue = "\"" + queryValue + "\"";
        } else{
            queryValue = QueryParser.escape(queryValue);
        }
        QueryParser parser = new QueryParser(aField.name(), wrapper);
        Query q  = parser.parse(queryValue);
        return q;
    }

    private void printDoc(Document doc, int rank, double score, int id){
        String authors = "";
        for (IndexableField authorField : doc.getFields(myFields.authors.name())) {
            authors += authorField.stringValue() + ", ";
        }
        if (authors.length() > 2)
            authors = authors.substring(0, authors.length() - 2);

        String paperId = doc.get(myFields.paper_id.name());
        String title = doc.get(myFields.title.name());
        String venue = doc.get(myFields.venue.name());
        String type = doc.get(myFields.type.name());
        int year = Integer.parseInt(doc.get(myFields.year.name()));

        if (title != null) {
            System.out.println("\n\n--------------------------------------");
            System.out.println("authors : " + authors + "\n" +
                            "key :\t" + paperId + "\n" +
                            "title :\t" + title + "\n" +
                            "venue :\t" + venue + "\n" +
                            "year :\t" + year + "\n" +
                            "type : \t" + type + "\n" +
                            "rank :\t" + rank + "\n" +
                            "score :\t" + score + "\n" +
                            "id :\t" + id
            );
        } else {
            System.out.println(rank + ". " + "No title for this document");
        }

    }

    private static String removeRepated(String str){
        return str.replaceAll("([.!?:;,<>?|()'\"|\\\\])(?=\\1)", "");
    }

    private String removeNumbers(String s){
        s = s.replaceAll("[0-9]", "");
        return s;
    }


}