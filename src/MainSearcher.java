import luceneInd.SearchIndex;
import org.apache.lucene.queryparser.classic.ParseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Sandro on 15/3/16.
 * Run this class to query the index.
 * It implement a UI interface the makes easier for the user to specify the query
 */
public class MainSearcher extends JFrame implements ActionListener {
    //Declare UI component
    private Label lblAuthor;    // Declare component Label
    private TextField tfAuthor; // Declare component TextField
    private JCheckBox checkboxAuthors;
    private boolean editableAuthors = false;

    private Label lblPaper_id;    // Declare component Label
    private TextField tfPaper_id; // Declare component TextField
    private JCheckBox checkboxPaper_id;
    private boolean editablePaper_id = false;


    private Label lblTitle;    // Declare component Label
    private TextField tfTitle; // Declare component TextField
    private JCheckBox checkboxTitle;
    private boolean editableTitle = false;


    private Label lblYear;    // Declare component Label
    private TextField tfYear; // Declare component TextField
    private JCheckBox checkboxYear;
    private boolean editableYear = false;


    private Label lblVenue;    // Declare component Label
    private TextField tfVenue; // Declare component TextField
    private JCheckBox checkboxVenue;
    private boolean editableVenue = false;


    private Label lblType;    // Declare component Label
    private TextField tfType; // Declare component TextField
    private JCheckBox checkboxType;
    private boolean editableType = false;


    private JCheckBox checkboxPagination;

    private TextField tfTopN; // Declare component TextField



    private Button btnSearch;   // Declare component Button


    //Declare analyzer properties
    private static boolean useStopword;
    private static boolean usePorterStemmer;
    private static boolean interactive = false;
    private static boolean useLowercaseNormalizatio;

    private static String indexBasePath;
    private static boolean rawOutputFormat;
    private static int topN = 10;


    // Constructor to setup the GUI components
    public MainSearcher(){
        setLayout(new GridLayout(0, 3));
        // "super" Frame sets its layout to FlowLayout, which arranges the components
        //  from left-to-right, and flow to next row from top-to-bottom.

        for (SearchIndex.myFields aField : SearchIndex.myFields.values()){
            switch (aField){
                case paper_id:
                    checkboxPaper_id = new JCheckBox(aField.name());
                    checkboxPaper_id.setMnemonic(KeyEvent.VK_P);
                    checkboxPaper_id.addItemListener(new ItemListener() {
                        public void itemStateChanged(ItemEvent e) {
                            editablePaper_id = !editablePaper_id;
                            tfPaper_id.setEditable(editablePaper_id);
                        }
                    });
                    add(checkboxPaper_id);

                    lblPaper_id = new Label("Paper_id");  // construct Label
                    add(lblPaper_id);                    // "super" Frame adds Label

                    tfPaper_id = new TextField("", 30); // construct TextField
                    tfPaper_id.setEditable(editablePaper_id);       // set to read-only
                    add(tfPaper_id);
                    break;
                case authors:
                    checkboxAuthors = new JCheckBox(aField.name());
                    checkboxAuthors.setMnemonic(KeyEvent.VK_A);
                    checkboxAuthors.addItemListener(new ItemListener() {
                        public void itemStateChanged(ItemEvent e) {
                            editableAuthors = !editableAuthors;
                            tfAuthor.setEditable(editableAuthors);
                        }
                    });
                    add(checkboxAuthors);
                    
                    lblAuthor = new Label("Author");  // construct Label
                    add(lblAuthor);                    // "super" Frame adds Label

                    tfAuthor = new TextField("", 30); // construct TextField
                    tfAuthor.setEditable(editableAuthors);       // set to read-only
                    add(tfAuthor);                     // "super" Frame adds tfCount
                    break;
                case title:
                    checkboxTitle = new JCheckBox(aField.name());
                    checkboxTitle.setMnemonic(KeyEvent.VK_T);
                    checkboxTitle.addItemListener(new ItemListener() {
                        public void itemStateChanged(ItemEvent e) {
                            editableTitle = !editableTitle;
                            tfTitle.setEditable(editableTitle);
                        }
                    });
                    add(checkboxTitle);

                    lblTitle = new Label("Title");  // construct Label
                    add(lblTitle);                    // "super" Frame adds Label

                    tfTitle = new TextField("", 30); // construct TextField
                    tfTitle.setEditable(editableTitle);       // set to read-only
                    add(tfTitle);
                    break;
                case year:
                    checkboxYear = new JCheckBox(aField.name());
                    checkboxYear.setMnemonic(KeyEvent.VK_Y);
                    checkboxYear.addItemListener(new ItemListener() {
                        public void itemStateChanged(ItemEvent e) {
                            editableYear = !editableYear;
                            tfYear.setEditable(editableYear);
                        }
                    });
                    add(checkboxYear);

                    lblYear = new Label("Year");  // construct Label
                    add(lblYear);                    // "super" Frame adds Label

                    tfYear = new TextField("", 30); // construct TextField
                    tfYear.setEditable(editableYear);       // set to read-only
                    add(tfYear);
                    break;
                case venue:
                    checkboxVenue = new JCheckBox(aField.name());
                    checkboxVenue.setMnemonic(KeyEvent.VK_V);
                    checkboxVenue.addItemListener(new ItemListener() {
                        public void itemStateChanged(ItemEvent e) {
                            editableVenue = !editableVenue;
                            tfVenue.setEditable(editableVenue);
                        }
                    });
                    add(checkboxVenue);

                    lblVenue = new Label("Venue");  // construct Label
                    add(lblVenue);                    // "super" Frame adds Label

                    tfVenue = new TextField("", 30); // construct TextField
                    tfVenue.setEditable(editableVenue);       // set to read-only
                    add(tfVenue);
                    break;


                case type:
                    checkboxType = new JCheckBox(aField.name());
                    checkboxType.setMnemonic(KeyEvent.VK_T);
                    checkboxType.addItemListener(new ItemListener() {
                        public void itemStateChanged(ItemEvent e) {
                            editableType = !editableType;
                            tfType.setEditable(editableType);
                        }
                    });
                    add(checkboxType);

                    lblType = new Label("Type");  // construct Label
                    add(lblType);                    // "super" Frame adds Label

                    tfType = new TextField("", 30); // construct TextField
                    tfType.setEditable(editableType);       // set to read-only
                    add(tfType);
                    break;
            }
        }


        checkboxPagination = new JCheckBox("interactive");
        checkboxPagination.setMnemonic(KeyEvent.VK_I);
        checkboxPagination.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                interactive = !interactive;
            }
        });
        add(checkboxPagination);

        add(new Label("Results pagination"));
        add(new Label(""));


        tfTopN = new TextField("", 30); // construct TextField
        tfTopN.setEditable(true);
        add(new Label(""));
        add(new Label("Top-N"));
        add(tfTopN);



        btnSearch = new Button("Search");   // construct Button
        add(btnSearch);                    // "super" Frame adds Button

        btnSearch.addActionListener(this);
        // Clicking Button fires ActionEvent
        // btnSearch registers this instance as ActionEvent listener

        setTitle("Search fields");  // "super" Frame sets title
        setSize(470, 300);        // "super" Frame sets initial window size

        setVisible(true);         // "super" Frame shows

    }

    public static void main(String[] args) throws IOException, ParseException {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            //read the analyzer properties
            input = new FileInputStream("config.properties");
            prop.load(input);

            useStopword = Boolean.parseBoolean(prop.getProperty("useStopword"));
            usePorterStemmer = Boolean.parseBoolean(prop.getProperty("usePorterStemmer"));
            useLowercaseNormalizatio = Boolean.parseBoolean(prop.getProperty("useLowercaseNormalizatio"));

            indexBasePath = prop.getProperty("indexBasePath");
            rawOutputFormat = Boolean.parseBoolean(prop.getProperty("rawOutputFormat"));
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        // Invoke the constructor (to setup the GUI) by allocating an instance
        MainSearcher app = new MainSearcher();
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        //read the query
        boolean quit = false;
        Map<SearchIndex.myFields, Object> fields = new HashMap<>();

        if (editableAuthors)
            fields.put(SearchIndex.myFields.authors, tfAuthor.getText().trim());
        if (editablePaper_id)
            fields.put(SearchIndex.myFields.paper_id, tfPaper_id.getText().trim());
        if (editableTitle)
            fields.put(SearchIndex.myFields.title, tfTitle.getText().trim());
        if (editableVenue)
            fields.put(SearchIndex.myFields.venue, tfVenue.getText().trim());
        if (editableYear){
            try{
                fields.put(SearchIndex.myFields.year, Integer.parseInt(tfYear.getText().trim()));
            }catch (NumberFormatException er){
                System.err.println("Error in the year input");
            }
        }
        if (editableType)
            fields.put(SearchIndex.myFields.type, tfType.getText().trim());

        try{
            topN = Integer.parseInt(tfTopN.getText().trim());
        }catch (NumberFormatException er){
            //default config
            topN = 10;
            System.err.println("Error in the top N input");
        }

        //Execute the query
        SearchIndex searchIndex = new SearchIndex( indexBasePath, useStopword, usePorterStemmer, useLowercaseNormalizatio);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
        try {
            quit = searchIndex.doSearch(fields, topN, rawOutputFormat, interactive, in);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (quit){
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.exit(0);
        }
    }
}
