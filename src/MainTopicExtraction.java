import DAO.Document;
import DAO.YearVocabularyAndDocs;
import luceneInd.SearchIndex;

import java.io.*;
import java.util.List;
import java.util.Properties;

/**
 * Created by Sandro on 22/3/16.
 * Main class used to extract the documents to feed the BTM topic model
 */
public class MainTopicExtraction {
    private static Properties property;
    private static SearchIndex searchIndex;
    private static int NUM_TOPIC = 10;

    private static boolean useStopword;
    private static boolean usePorterStemmer;
    private static boolean useLowercaseNormalizatio;
    private static String indexBasePath;

    public static void main(String[] args) throws Exception {

        //read properties
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            property = new Properties();
            property.load(input);

            useStopword = Boolean.parseBoolean(property.getProperty("useStopword"));
            usePorterStemmer = Boolean.parseBoolean(property.getProperty("usePorterStemmer"));
            useLowercaseNormalizatio = Boolean.parseBoolean(property.getProperty("useLowercaseNormalizatio"));
            indexBasePath = property.getProperty("indexBasePath");

        } catch (IOException ex) {
            throw ex;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        searchIndex = new SearchIndex(indexBasePath, useStopword, usePorterStemmer, useLowercaseNormalizatio);
        List<YearVocabularyAndDocs> yearsVocabulary = searchIndex.getVocabularyDocument(SearchIndex.myFields.title, 2000, 2015);

        for (YearVocabularyAndDocs yearData : yearsVocabulary){

            System.out.println("Totoal document to model for year : " + yearData.getYear() +" is " + yearData.getYearDocuments().size());
            String file_name = writeDocs(yearData.getYearDocuments(), yearData.getYear());

            //String pathModel = property.getProperty("modelsPath");
            //String pathTopic = property.getProperty("topicPath");
            //TopicExtractor.run(NUM_TOPIC, yearData.getYear(), pathModel +File.separator + yearData.getYear(), pathTopic + File.separator + yearData.getYear(), yearData.getYearsVocabulary(), yearData.getYearDocuments());
        }

        /*
        try{
            Process p = Runtime.getRuntime().exec("chmod 777 " + "BTMc" + File.separator + "script" + File.separator + "runExample.sh" );
            p.waitFor();

            ProcessBuilder pb = new ProcessBuilder("BTMc" + File.separator + "script" + File.separator + "runExample.sh");
            p = pb.start();
            p.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                System.out.println(line);
            }

        }catch( IOException ex ){
            //Validate the case the file can't be accesed (not enought permissions)
            throw ex;
        }
        */
    }


    private static String writeDocs(List<Document> documents, int year){
        String fileName = "doc_"+year+".txt";
        try {
            File file = new File("BTMc" + File.separator + "sample-data" + File.separator +fileName);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            for (Document doc : documents){
                bw.write(doc.getContent() + "\n");
            }
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
    }
}

