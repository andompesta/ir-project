package parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandro on 3/14/16.
 * class that contains the paper information
 */
public class PaperInfo implements Comparable{
    public static final int ARTICLE  = 1;
    public static final int INPROCEEDINGS = 2;

    private String key;
    private String title;
    private List<String> authors;
    private int year;
    private String venue;
    private String type;
    public double score;

    public PaperInfo() {
        this.authors = new ArrayList<String>();
    }

    public PaperInfo(String key, String title, List<String> authors, int year, String venue, String type) {
        this.key = key;
        this.title = title;
        this.authors = authors;
        this.year = year;
        this.venue = venue;
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public void addAuthors(String author){
        this.authors.add(author);
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) throws Exception {
        this.type = type;
    }

    @Override
    public String toString() {
        return "key='" + key + "\t " +
                "title='" + title + "\t " +
                "authors=" + authors + "\t " +
                "year=" + year + "\t " +
                "venue='" + venue + "\t " +
                "type=" + type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaperInfo paperInfo = (PaperInfo) o;

        if (key != null ? !key.equals(paperInfo.key) : paperInfo.key != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return key != null ? key.hashCode() : 0;
    }

    @Override
    public int compareTo(Object o) {
        o = (PaperInfo) o;
        return this.getKey().compareTo(((PaperInfo) o).getKey());
    }
}
