package parser;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Sandro on 3/14/16.
 * SAX based pareser for dblp
 */
public class Parser {
    private static List<PaperInfo> paperInfoToSave;

    private class ConfigHandler extends DefaultHandler {
        private Locator locator;

        private PaperInfo aPaperInfo;

        private String person_name;
        private String element;
        private String recordTag;
        private String prevElement = "";

        private boolean insidePerson;
        private boolean insideElement;


        public void setDocumentLocator(Locator locator) {
            this.locator = locator;
        }

        public void startElement(String namespaceURI, String localName, String rawName, Attributes atts) throws SAXException {

            /*if (infoToSave.size() == 30000){
                try {
                    indexer.openWriter(true);
                    indexer.indexDocument(infoToSave);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        indexer.closeWriter();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                infoToSave.clear();
            }*/
            //extract the information needed when the element is opened
            if (rawName.equals("article") || rawName.equals("inproceedings") ){
                String key;
                if ((atts.getLength() > 0) && ((key = atts.getValue("key")) != null)) {
                    aPaperInfo = new PaperInfo();
                    try {
                        aPaperInfo.setType(rawName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    aPaperInfo.setKey(key);
                    recordTag = rawName;
                }
                insidePerson = false;
                insideElement = false;
            } else if (insidePerson = (rawName.equals("author") || rawName.equals("editor"))) {
                person_name = "";
                insideElement = false;
            } else if (insideElement = (rawName.equals("title") || rawName.equals("year") || rawName.equals("booktitle") || rawName.equals("journal")) ) {
                element = "";
                insidePerson = false;
            } else if (insideElement = (rawName.equals("i") || rawName.equals("sub") || rawName.equals("sup") || rawName.equals("tt") || rawName.equals("reg") || rawName.equals("ref") || rawName.equals("micro") || rawName.equals("times") )){
                prevElement = element;
                element = "";
            }else {
                insidePerson = false;
                insideElement = false;
            }
        }

        public void endElement(String namespaceURI, String localName, String rawName) throws SAXException {
            //extract the information needed when the element is closed
            if (insidePerson) {
                aPaperInfo.addAuthors(person_name);
            } else if(insideElement){
                if (rawName.equals("title")){
                    aPaperInfo.setTitle(element);
                } else if (rawName.equals("year")){
                    aPaperInfo.setYear(Integer.parseInt(element));
                } else if (rawName.equals("booktitle") || rawName.equals("journal") ){
                    aPaperInfo.setVenue(element);
                } else if (rawName.equals("i") || rawName.equals("sub") || rawName.equals("sup") || rawName.equals("tt") || rawName.equals("reg") || rawName.equals("ref") || rawName.equals("micro") || rawName.equals("times")){
                    element = prevElement + element;
                }
            } else if (rawName.equals(recordTag)) {
                paperInfoToSave.add(aPaperInfo);
            }
        }

        //get the value of an element
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (insidePerson)
                person_name += new String(ch, start, length);
            else if (insideElement)
                element += new String(ch, start, length);
        }

        private void Message(String mode, SAXParseException exception) {
            System.out.println(mode + " Line: " + exception.getLineNumber()
                    + " URI: " + exception.getSystemId() + "\n" + " Message: "
                    + exception.getMessage());
        }

        public void warning(SAXParseException exception) throws SAXException {

            Message("**Parsing Warning**\n", exception);
            throw new SAXException("Warning encountered");
        }

        public void error(SAXParseException exception) throws SAXException {

            Message("**Parsing Error**\n", exception);
            throw new SAXException("Error encountered");
        }

        public void fatalError(SAXParseException exception) throws SAXException {

            Message("**Parsing Fatal Error**\n", exception);
            throw new SAXException("Fatal Error encountered");
        }
    }


    public Parser(String uri, List<PaperInfo> paperInfos) {
        paperInfoToSave = paperInfos;
        try {
            SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            SAXParser parser = parserFactory.newSAXParser();
            ConfigHandler handler = new ConfigHandler();
            parser.getXMLReader().setFeature("http://xml.org/sax/features/validation", true);
            parser.parse(new File(uri), handler);
        } catch (IOException e) {
            System.out.println("Error reading URI: " + e.getMessage());
        } catch (SAXException e) {
            System.out.println("Error in parsing: " + e.getMessage());
        } catch (ParserConfigurationException e) {
            System.out.println("Error in XML parser configuration: " +
                    e.getMessage());
        }
    }
}
