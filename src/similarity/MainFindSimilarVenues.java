package similarity;


import luceneInd.SearchIndex;
import org.apache.lucene.search.spell.NGramDistance;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.List;

/**
 * Created by jeffery guo on 31/3/16.
 */
public class MainFindSimilarVenues extends JFrame implements ActionListener {

    private Label lblYear;    // Declare component Label
    private TextField tfYear; // Declare component TextField
    private int year;
    //private JCheckBox checkboxYear;
    //private boolean editableYear = false;


    private Label lblVenue;    // Declare component Label
    private TextField tfVenue; // Declare component TextField
    private String venue;
    //private JCheckBox checkboxVenue;
    //private boolean editableVenue = false;

    private Button btnSearch;   // Declare component Button

    private static boolean useStopword;
    private static boolean usePorterStemmer;
    private static boolean useLowercaseNormalizatio;
    private static String indexBasePath;
    private static boolean rawOutputFormat;

    // Constructor to setup the GUI components
    public MainFindSimilarVenues() throws Exception {
        setLayout(new GridLayout(0, 2));

        /*
        checkboxYear = new JCheckBox(SearchIndex.myFields.year.name());
        checkboxYear.setMnemonic(KeyEvent.VK_Y);
        checkboxYear.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                editableYear = !editableYear;
                tfYear.setEditable(editableYear);
            }
        });
        add(checkboxYear);
        */

        lblVenue = new Label("Venue");  // construct Label
        add(lblVenue);                    // "super" Frame adds Label

        tfVenue = new TextField("", 30); // construct TextField
        tfVenue.setEditable(true);       // set to read-only
        add(tfVenue);



        lblYear = new Label("Year");  // construct Label
        add(lblYear);                    // "super" Frame adds Label

        tfYear = new TextField("", 30); // construct TextField
        tfYear.setEditable(true);       // set to read-only
        add(tfYear);
        /*
        checkboxVenue = new JCheckBox(SearchIndex.myFields.venue.name());
        checkboxVenue.setMnemonic(KeyEvent.VK_V);
        checkboxVenue.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                editableVenue = !editableVenue;
                tfVenue.setEditable(editableVenue);
            }
        });
        add(checkboxVenue);
        */



        btnSearch = new Button("Search");   // construct Button
        add(btnSearch);                    // "super" Frame adds Button

        btnSearch.addActionListener(this);
        // Clicking Button fires ActionEvent
        // btnSearch registers this instance as ActionEvent listener

        setTitle("Search fields");  // "super" Frame sets title
        setSize(470, 300);        // "super" Frame sets initial window size

        setVisible(true);         // "super" Frame shows

    }

    public static void main(String[] args) throws Exception {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            //read the analyzer properties
            input = new FileInputStream("config.properties");
            prop.load(input);

            useStopword = Boolean.parseBoolean(prop.getProperty("useStopword"));
            usePorterStemmer = Boolean.parseBoolean(prop.getProperty("usePorterStemmer"));
            useLowercaseNormalizatio = Boolean.parseBoolean(prop.getProperty("useLowercaseNormalizatio"));

            indexBasePath = prop.getProperty("indexBasePath");
            rawOutputFormat = Boolean.parseBoolean(prop.getProperty("rawOutputFormat"));
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Invoke the constructor (to setup the GUI) by allocating an instance
    	MainFindSimilarVenues app = new MainFindSimilarVenues();
    }
    
    
    @Override
    public void actionPerformed(ActionEvent event) {
        boolean quit = false;
        List<String> paperTitles;
        //create query map
        Venue query = null;
        try{
            String venueName = tfVenue.getText().trim();
            int year = Integer.parseInt(tfYear.getText().trim());
            query = new Venue(venueName, year);
        }catch (NumberFormatException er){
            System.err.println("Error in the year input");
            System.exit(-1);
        }

        /*Map<SearchIndex.myFields, Object> fields = new HashMap<>();
        fields.put(SearchIndex.myFields.venue, tfVenue.getText());
            try{
                fields.put(SearchIndex.myFields.year, Integer.parseInt(tfYear.getText()));
            }catch (NumberFormatException er){
                System.err.println("Error in the year input");
            }
        }
        */

        SearchIndex searchPapers = new SearchIndex(indexBasePath, useStopword, usePorterStemmer, useLowercaseNormalizatio);

        try {
            searchPapers.openIndex();
        	System.out.println("\n*------------------------------------------------------------*");
        	System.out.println("Your query is: " + query.toString() + "\n");

            //searchPapers.doSearch(fields, repeat, rawOutputFormat, interactive, in);
            //int paperNumber = searchPapers.getPaperNumber();
            //if(paperNumber>0){

            paperTitles = searchPapers.getPaperTitles(query);
            System.out.println("*Start to retrieve top 10 similar venues which may take 10-50s\n");
            long tStart = System.currentTimeMillis();
            String qTitles = String.join(",", paperTitles);


            List<Venue> candidates = new ArrayList<>();
            //List<String> venueNames = new ArrayList<String>();
            //Collections.shuffle(paperTitles);


            for(String title : paperTitles){

                if (candidates.size() > 50)
                    break;

                Set<Venue> candidateVenues = (Set<Venue>) searchPapers.getPaperVenue(title);
                for (Venue potCandidateVenue : candidateVenues){
                    if ( ! candidates.contains( potCandidateVenue) ){
                        candidates.add(potCandidateVenue);
                    }
                }
            }
            //get candidate venues

            System.out.println("*There are " + candidates.size()+" candidates\n");

            //get candidate titles
            int i = 0;
            for(Venue candidateVenue : candidates){
                i += 1;
                if (i == Math.min(100, candidates.size())){
                    break;
                }

                List<String> paperTitles2 = searchPapers.getPaperTitles(candidateVenue);
                String qTitles2 = String.join(",", paperTitles2);
                //compute similarity
                NGramDistance ngram = new NGramDistance(3);
                double sim = ngram.getDistance(qTitles, qTitles2);
                candidateVenue.setScore(sim);
            }
            Collections.sort(candidates, Collections.reverseOrder());

            candidates = candidates.subList(0, 11);




            long tEnd = System.currentTimeMillis();
            System.out.println("\nTime " + (tEnd - tStart) + " total milliseconds\n");


            for (Venue venue : candidates){
                System.out.println(venue.toString());
            }

        } catch (Exception e) {
        	e.printStackTrace();
        }
        finally {
            try {
                searchPapers.closeIndex();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
