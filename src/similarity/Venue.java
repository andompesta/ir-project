package similarity;

/**
 * Created by ando on 30/3/16.
 */
public class Venue implements Comparable{
    public String venueName;
    public int year;
    public double score;

    public Venue(String venueName, int year) {
        this.venueName = venueName;
        this.year = year;
        score = 0;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Venue venue = (Venue) o;

        if (year != venue.year) return false;
        return venueName != null ? venueName.equals(venue.venueName) : venue.venueName == null;

    }

    @Override
    public int hashCode() {
        int result = venueName != null ? venueName.hashCode() : 0;
        result = 31 * result + year;
        return result;
    }

    @Override
    public int compareTo(Object o) {
        o = (Venue) o;

        return Double.compare(this.score, ((Venue) o).score);
    }

    @Override
    public String toString() {
        return venueName + " \t" + year + " \t" + score;
    }
}
