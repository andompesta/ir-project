import luceneInd.IndexingFiles;
import parser.PaperInfo;
import parser.Parser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Sandro on 15/3/16.
 * Run this class to create the lucene index.
 */
public class MainIndexer {

    private static IndexingFiles indexer;
    private static List<PaperInfo> paperInfoToSave = new ArrayList<PaperInfo>();
    private static final boolean INDEXING = false;

    public static void main(String[] args) throws Exception {
        Properties prop = new Properties();
        InputStream input = null;

        input = new FileInputStream("config.properties");
        prop.load(input);
        //read indexing property
        boolean useStopword = Boolean.parseBoolean(prop.getProperty("useStopword"));
        boolean usePorterStemmer = Boolean.parseBoolean(prop.getProperty("usePorterStemmer"));
        boolean updateExisting = Boolean.parseBoolean(prop.getProperty("updateExisting"));
        boolean useLowercaseNormalizatio = Boolean.parseBoolean(prop.getProperty("useLowercaseNormalizatio"));
        String indexPath = prop.getProperty("indexBasePath");

        //parse the xml file
        Parser p = new Parser(indexPath + File.separator + "dblp.xml", paperInfoToSave);



        //create the lucene index
        indexer  = new IndexingFiles(indexPath);
        try {
            //index all documents
            indexer.openWriter(updateExisting, useStopword, usePorterStemmer, useLowercaseNormalizatio);
            indexer.indexDocument(paperInfoToSave);
        } catch (IOException e) {
            throw e;
        } finally {
            indexer.closeWriter();
        }

        if (input != null) {
            input.close();
        }
    }
}
