package DAO;

import java.util.List;

/**
 * Created by Sandro on 3/3/15.
 */
public class Document {
    private String content;
    private List<Integer> wordList;
    private int _idDoc;


    public Document(List<Integer> wordList, int _idDoc) {
        this.wordList = wordList;
        this._idDoc = _idDoc;
    }

    public Document(List<Integer> wordList, int _idDoc, String content) {
        this.wordList = wordList;
        this._idDoc = _idDoc;
        this.content = content;
    }

    public Document() {
    }

    public int get_w(int i) throws Exception {
        if (i < wordList.size()){
            return wordList.get(i);
        }else{
            throw new Exception("Index in document with a single word");
        }
    }
    /*public Document(int id, String s) {
        _idDoc = id;
        this.wordList = new ArrayList<Integer>();
        readDocument(s);
    }*/

    private void readDocument(String s){
        String[] tokens = s.split(" ");
        for (int i = 0; i < tokens.length; i++){
            wordList.add(Integer.parseInt(tokens[i]));
        }
    }

    public void genBiterm(List<Biterm>bs, int win){
        if (win == 0)
            win = 50;
        if (wordList.size() < 2)
            return;

        if (win < wordList.size())
            System.err.println("Document: " + _idDoc + " have lot of biterm, some will be droped");

        for (int i = 0; i < wordList.size()-1; i++){
            for (int j = i+1; j < Math.min(i+win, wordList.size()); j++){
                bs.add( new Biterm(wordList.get(i), wordList.get(j)));
            }
        }
    }

    public int getWord(int index) throws Exception {
        if (index >= wordList.size()){
            throw new Exception("Error in the selection of the word, index "+ index + "out of bound");
        }
        return wordList.get(index);
    }

    public int size(){
        return this.wordList.size();
    }


    public List<Integer> getWordList() {
        return wordList;
    }

    public void setWordList(List<Integer> wordList) {
        this.wordList = wordList;
    }

    public int get_idDoc() {
        return _idDoc;
    }

    public void set_idDoc(int _idDoc) {
        this._idDoc = _idDoc;
    }

    public String getContent() {
        return content;
    }
}
