package DAO;

import luceneInd.SearchIndex;
import parser.PaperInfo;

import java.util.*;

/**
 * Created by Sandro on 3/25/16.
 */
public class QueryExtraction {

    public Map<SearchIndex.myFields, Object> query;
    public List<PaperInfo> queryResults;

    public QueryExtraction() {
        query = new HashMap<>();
        queryResults = new ArrayList<>();
    }

    public QueryExtraction(Map<SearchIndex.myFields, Object> query, List<PaperInfo> queryResults) {
        this.query = query;
        this.queryResults = queryResults;
    }
}
