package DAO;

import org.apache.commons.collections4.BidiMap;

import java.util.List;

/**
 * Created by Sandro on 22/3/16.
 */
public class YearVocabularyAndDocs {
    int year;
    BidiMap<String, Integer> yearsVocabulary;
    List<Document> yearDocuments;

    public YearVocabularyAndDocs(int year, BidiMap<String, Integer> yearsVocabulary, List<Document> yearDocuments) {
        this.year = year;
        this.yearsVocabulary = yearsVocabulary;
        this.yearDocuments = yearDocuments;
    }

    public YearVocabularyAndDocs() {
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public BidiMap<String, Integer> getYearsVocabulary() {
        return yearsVocabulary;
    }

    public void setYearsVocabulary(BidiMap<String, Integer> yearsVocabulary) {
        this.yearsVocabulary = yearsVocabulary;
    }

    public List<Document> getYearDocuments() {
        return yearDocuments;
    }

    public void setYearDocuments(List<Document> yearDocuments) {
        this.yearDocuments = yearDocuments;
    }
}
