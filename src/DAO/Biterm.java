package DAO;

/**
 * Created by Sandro on 3/3/15.
 */
public class Biterm {
    private int wi;
    private int wj;
    private int topic;

    public int getWi() {
        return wi;
    }

    public void setWi(int wi) {
        this.wi = wi;
    }

    public int getWj() {
        return wj;
    }

    public void setWj(int wj) {
        this.wj = wj;
    }

    public int getTopic() {
        return topic;
    }

    public void setTopic(int topic) {
        this.topic = topic;
    }

    public void resetTopic() {
        this.topic = -1;
    }

    public Biterm(int w1, int w2) {
        if (w1 < w2 ){
            this.wi = w1;
            this.wj = w2;
        }else{
            this.wi = w2;
            this.wj = w1;
        }
        this.topic = -1;
    }

    public Biterm() {
    }

    public Biterm(String s) {
        String[] tokens = s.split(" ");
        if (tokens.length == 3){
            this.wi = Integer.parseInt(tokens[0]);
            this.wj = Integer.parseInt(tokens[1]);
            this.topic = Integer.parseInt(tokens[2]);
        }
    }

    @Override
    public String toString() {
        return "Biterm{" +
                "wi=" + wi +
                ", wj=" + wj +
                ", topic=" + topic +
                '}';
    }
}
