package evaluation;

import DAO.QueryExtraction;
import com.google.gson.Gson;
import luceneInd.SearchIndex;
import parser.PaperInfo;
import parser.Parser;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Created by Sandro on 3/25/16.
 * Main class used to extract the ground true for the test query. Save the results in a json file
 */
public class MainExtractQuery {
    private static List<PaperInfo> paperInfoToSave = new ArrayList<PaperInfo>();

    public static void main(String[] args) throws Exception {
        Properties prop = new Properties();
        InputStream input = null;

        input = new FileInputStream("config.properties");
        prop.load(input);
        //read indexing property
        String indexPath = prop.getProperty("indexBasePath");

        Parser p = new Parser(indexPath + File.separator + "dblp.xml", paperInfoToSave);


        //init the results set
        QueryExtraction[] queries = new QueryExtraction[10];
        for (int i = 0; i < queries.length; i++) {
            queries[i] = new QueryExtraction();
        }

        //start to extract the ground true
        for (PaperInfo pi : paperInfoToSave){
            String authors = "";
            for (String author : pi.getAuthors()) {
                authors += author + " ";
            }
            //tokenize the authors fileds
            List<String> authorsTokens = Arrays.asList(authors.toLowerCase().split(" "));


            //extract the query
            if (authors.toLowerCase().contains("zheng") && (pi.getTitle().toLowerCase().toLowerCase().contains("machine learning") || pi.getTitle().toLowerCase().toLowerCase().contains("machine-learning")) ){
                queries[0].query.put(SearchIndex.myFields.authors , "Zheng");
                queries[0].query.put(SearchIndex.myFields.title , "\"machine learning\"");
                queries[0].queryResults.add(pi);
            }


            if ( (authorsTokens.contains("sanjeev") && authorsTokens.contains("saxena")) && pi.getType().equals("inproceedings")) {
                queries[1].query.put(SearchIndex.myFields.authors , "\"Sanjeev Saxena\"");
                queries[1].query.put(SearchIndex.myFields.type , "inproceedings");
                queries[1].queryResults.add(pi);
            }


            if ( ( (authorsTokens.contains("hans-ulrich") && authorsTokens.contains("simon")) || ( authorsTokens.contains("hans") && authorsTokens.contains("ulrich") && authorsTokens.contains("simon") ))
                    && pi.getType().equals("article") && pi.getTitle().toLowerCase().contains("pattern")){
                queries[2].query.put(SearchIndex.myFields.authors , "\"Hans-Ulrich Simon\"");
                queries[2].query.put(SearchIndex.myFields.type , "article");
                queries[2].query.put(SearchIndex.myFields.title , "Pattern");
                queries[2].queryResults.add(pi);
            }


            //title + year + venue + type: data mining, 2010, ieee, inproceedings
            if (pi.getTitle().toLowerCase().contains("data mining") &&
                    pi.getVenue().toLowerCase().contains("ieee") && pi.getYear() == 2010 ){
                queries[3].query.put(SearchIndex.myFields.title , "\"data mining\"");
                queries[3].query.put(SearchIndex.myFields.venue , "IEEE");
                queries[3].query.put(SearchIndex.myFields.year , 2010);
                queries[3].queryResults.add(pi);
            }


            //Author + title + year+ type: Zhang, robust model, 2015, article
            if (authorsTokens.contains("zhang") && pi.getTitle().toLowerCase().contains("robust") &&  pi.getYear() == 2015 &&
                    pi.getTitle().toLowerCase().contains("model") && pi.getType().toLowerCase().contains("article") ){

                queries[4].query.put(SearchIndex.myFields.authors , "Zhang");
                queries[4].query.put(SearchIndex.myFields.title , "robust model");
                queries[4].query.put(SearchIndex.myFields.type , "article");
                queries[4].query.put(SearchIndex.myFields.year , 2015);

                queries[4].queryResults.add(pi);
            }


            //Author + paper_id + title + year+ venue: Zhang, zhangz15a, robust model
            if (pi.getKey().toLowerCase().contains("zhangz15a") ){
                queries[5].query.put(SearchIndex.myFields.paper_id , "ZhangZ15a");
                queries[5].queryResults.add(pi);
            }

            //Author + year : sandro , 2000
            if (authorsTokens.contains("sandro") && pi.getYear() == 2000){
                queries[6].query.put(SearchIndex.myFields.authors , "Sandro");
                queries[6].query.put(SearchIndex.myFields.year , 2000);
                queries[6].queryResults.add(pi);
            }


            //Author + title: Zhang, fuzzy logic
            if (authorsTokens.contains("zhang") && pi.getTitle().toLowerCase().contains("fuzzy") && pi.getTitle().toLowerCase().contains("logic")){
                queries[7].query.put(SearchIndex.myFields.authors , "Zhang");
                queries[7].query.put(SearchIndex.myFields.title , "fuzzy logic");
                queries[7].queryResults.add(pi);
            }

            //Author + year type: Nathan, 1983, article
            if (authorsTokens.contains("nathan") && pi.getYear() == 1983 && pi.getType().contains("article")){
                queries[8].query.put(SearchIndex.myFields.authors , "Nathan");
                queries[8].query.put(SearchIndex.myFields.type , "article");
                queries[8].query.put(SearchIndex.myFields.year , 1983);
                queries[8].queryResults.add(pi);
            }

            //author Venue : Simon, Acta
            if (authorsTokens.contains("simon")  && pi.getVenue().toLowerCase().contains("acta")){
                queries[9].query.put(SearchIndex.myFields.authors , "Simon");
                queries[9].query.put(SearchIndex.myFields.venue , "Acta");
                queries[9].queryResults.add(pi);
            }
        }

        //print json results
        Gson jsonFactory = new Gson();
        String jsonObj = jsonFactory.toJson(queries);
        pringJson(indexPath + File.separator + "queries.json",jsonObj);

    }

    //print the results in a file
    private static void pringJson(String path, String jsonContent) throws IOException {
        File file = new File(path);

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(jsonContent);
        bw.close();

    }
}
