package evaluation;

import DAO.QueryExtraction;
import com.google.gson.Gson;
import luceneInd.SearchIndex;
import parser.PaperInfo;

import java.io.*;
import java.util.List;
import java.util.Properties;

/**
 * Created by Sandro on 3/25/16.
 * Main class used to execute the query used as ground true
 */
public class MainExecuteQuery {
    private static boolean useStopword;
    private static boolean usePorterStemmer;
    private static boolean useLowercaseNormalizatio;

    private static String indexBasePath;
    private static boolean rawOutputFormat;
    private static int topN = 10;




    public static void main(String[] args) throws Exception{
        Properties prop = new Properties();
        InputStream input = null;

        Gson jsonFactory = new Gson();

        try {
            input = new FileInputStream("config.properties");
            prop.load(input);

            useStopword = Boolean.parseBoolean(prop.getProperty("useStopword"));
            usePorterStemmer = Boolean.parseBoolean(prop.getProperty("usePorterStemmer"));
            useLowercaseNormalizatio = Boolean.parseBoolean(prop.getProperty("useLowercaseNormalizatio"));

            indexBasePath = prop.getProperty("indexBasePath");
            rawOutputFormat = Boolean.parseBoolean(prop.getProperty("rawOutputFormat"));



            String jsonContent = readFile(indexBasePath + File.separator + "queries.json");
            //read the query used as ground true
            QueryExtraction[] queries = jsonFactory.fromJson(jsonContent, QueryExtraction[].class);

            SearchIndex searchIndex = new SearchIndex( indexBasePath, useStopword, usePorterStemmer, useLowercaseNormalizatio);


            QueryExtraction[] queriesRetrieved = new QueryExtraction[10];


            for (int i = 0; i<queries.length; i++){
                //esecute the query using the search engine
                List<PaperInfo> queryRetrival = searchIndex.doSearch(queries[i].query);
                queriesRetrieved[i] = new QueryExtraction(queries[i].query, queryRetrival);
            }

            //save output
            pringJson(indexBasePath + File.separator + "queriesRetrieved.json", jsonFactory.toJson(queriesRetrieved));

            System.out.println("Vocabulary dimension: " + searchIndex.getVocabulary(SearchIndex.myFields.title));

        } catch (IOException ex) {
            throw ex;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private static String readFile(String path) throws IOException {
        String content = "";

        BufferedReader br = new BufferedReader(new FileReader(path));
        String line = "";
        while ((line = br.readLine()) != null) {
            content += line;
        }
        return content;
    }

    //print the results in a file
    private static void pringJson(String path, String jsonContent) throws IOException {
        File file = new File(path);

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(jsonContent);
        bw.close();

    }
}
