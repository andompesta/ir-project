package evaluation;

import DAO.QueryExtraction;
import com.google.gson.Gson;
import luceneInd.SearchIndex;
import parser.PaperInfo;

import java.io.*;
import java.util.Collections;
import java.util.Properties;

/**
 * Created by Sandro on 3/25/16.
 * Main class used to compute the precision, racall, F1 scores
 */
public class MainEvaluation {

    public static void main(String[] args) throws Exception{

        Properties prop = new Properties();
        InputStream input = null;

        input = new FileInputStream("config.properties");
        prop.load(input);
        //read indexing property
        String indexPath = prop.getProperty("indexBasePath");


        Gson jsonFactory = new Gson();

        QueryExtraction[] queries = jsonFactory.fromJson(readFile(indexPath + File.separator + "queries.json"), QueryExtraction[].class);
        QueryExtraction[] queriesRetreived = jsonFactory.fromJson(readFile(indexPath + File.separator  + "queriesRetrieved.json"), QueryExtraction[].class);

        for (int i = 0; i < queries.length; i++) {

            Collections.sort(queries[i].queryResults);
            Collections.sort(queriesRetreived[i].queryResults);

            /*
            if (i == 5){
                for (PaperInfo pf : queries[i].queryResults){
                    System.out.println(pf.toString());
                }

                System.out.println("\n\n\n");

                for (PaperInfo pf : queriesRetreived[i].queryResults){
                    System.out.println(pf.toString());
                }
            }
            */





            int min = Math.min(queries[i].queryResults.size(), queriesRetreived[i].queryResults.size());
            min = Math.min(min, 10);
            double numRelevantRetreived = 0;
            double retreivedDocs = min;
            double relevantDocs = Math.min(10, queries[i].queryResults.size());

            System.out.println("retreived docs: " + retreivedDocs + "\t relevant docs" + relevantDocs );

            for (int j = 0; j < min; j++) {
                PaperInfo pi = queriesRetreived[i].queryResults.get(j);
                if (queries[i].queryResults.contains(pi)){
                    numRelevantRetreived += 1;
                }
                else {
                    System.out.println("\tERROR --\t" + j + "\t " + pi.toString());
                }


            }

            double precision = numRelevantRetreived / retreivedDocs;
            double recall = numRelevantRetreived / relevantDocs;
            double f1 = f1(precision, recall);


            System.out.println("queryNr " + i + "\t query" + queriesRetreived[i].query.get(SearchIndex.myFields.luceneQuery) + "\t precision: " + precision + "\t recall: " + recall + "\t f1 : " + f1);
            System.out.println("\n\n\n\n");
        }


    }

    private static double f1(double precision, double recall){
        return ( 2 * (precision * recall) / (precision + recall));
    }

    private static String readFile(String path) throws IOException {
        String content = "";

        BufferedReader br = new BufferedReader(new FileReader(path));
        String line = "";
        while ((line = br.readLine()) != null) {
            content += line;
        }
        return content;
    }
}
