create a folder dataset containing dblp.xml and dblp.dtd
config.properties: is the property file used to correctly setup the analyzer and the index directory

MainIndexer: main class is used to create the lucene index. It use the IndexingFiles class to specify how the fields are indexed. It output the time requred for indexing:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Indexing to directory 'dataset/index
indexing using stopwords
indexing with lowercase
Time 110515 total milliseconds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MainSearcher: main class to query the index. It implement a UI interface the makes easier for the user to specify the query. It user the SearchIndex class to actually execute the query.
Example for query : authors:sandro
Note that since the configuration was interactive false, no input were asked for the pagination
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
open index
Execute query: +authors:sandro
1607 total matching documents


--------------------------------------
authors : Sandro Zucchi
key :	journals/llc/Zucchi12
title :	Formal Semantics of Sign Languages.
venue :	Language and Linguistics Compass
year :	2012
type : 	article
rank :	0
score :	5.371275424957275
id :	5462


--------------------------------------
authors : Sandro Morasca
key :	journals/ese/Morasca97
title :	Applying QIP/GQM in a Maintenance Project.
venue :	Empirical Software Engineering
year :	1997
type : 	article
rank :	1
score :	5.371275424957275
id :	15430


--------------------------------------
authors : Sandro Tsang
key :	journals/ijkm/Tsang13
title :	Knowledge Representation Strategy Determination in Quantitative Terms.
venue :	IJKM
year :	2013
type : 	article
rank :	2
score :	5.371275424957275
id :	95571


--------------------------------------
authors : Sandro Zampieri
key :	journals/automatica/Zampieri97
title :	A behavioral approach to identifiability of 2D scalar systems.
venue :	Automatica
year :	1997
type : 	article
rank :	3
score :	5.371275424957275
id :	316038


--------------------------------------
authors : Sandro Bosio
key :	journals/4or/Bosio08
title :	On a new class of nonlinear set covering problems arising in wireless network design.
venue :	4OR
year :	2008
type : 	article
rank :	4
score :	5.371275424957275
id :	530463


--------------------------------------
authors : Sandro Zagatti
key :	journals/siamco/Zagatti00
title :	Minimization of Functionals of the Gradient by Baire's Theorem.
venue :	SIAM J. Control and Optimization
year :	2000
type : 	article
rank :	5
score :	5.371275424957275
id :	557717


--------------------------------------
authors : Sandro Bimonte
key :	journals/isi/Bimonte14
title :	A generic geovisualization model for spatial OLAP and its implementation in a standards-based architecture.
venue :	Ingénierie des Systèmes d'Information
year :	2014
type : 	article
rank :	6
score :	5.371275424957275
id :	572570


--------------------------------------
authors : Sandro Bologna
key :	journals/jss/Bologna97
title :	Guest editor's corner achieving quality in software.
venue :	Journal of Systems and Software
year :	1997
type : 	article
rank :	7
score :	5.371275424957275
id :	649661


--------------------------------------
authors : Sandro Zampieri
key :	journals/kybernetika/Zampieri94
title :	Exact modelling of scalar 2D arrays.
venue :	Kybernetika
year :	1994
type : 	article
rank :	8
score :	5.371275424957275
id :	663178


--------------------------------------
authors : Sandro Morasca
key :	conf/fase/Morasca03
title :	Foundations of a Weak Measurement-Theoretic Approach to Software Measurement.
venue :	FASE
year :	2003
type : 	inproceedings
rank :	9
score :	5.371275424957275
id :	781195
closed inproceedings index 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


MainExtractQuery: Main class used to extract the ground true for the test query. Save the results in a json file

MainExecuteQuery: Main class used to execute the query used as ground true
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
open index
Execute query: +title:"machine learning" +authors:zheng
46 total matching documents
closed inproceedings index 

open index
Execute query: +type:inproceedings +authors:"sanjeev saxena"
9 total matching documents
closed inproceedings index 

open index
Execute query: +type:article +title:pattern +authors:"hans ulrich simon"
2 total matching documents
closed inproceedings index 

open index
Execute query: +venue:ieee +title:"data mining" +year:[2010 TO 2010]
19 total matching documents
closed inproceedings index 

Execute query: +type:article +(title:robust title:model) +year:[2015 TO 2015] +authors:zhang
open index
491 total matching documents
closed inproceedings index 

open index
Execute query: +paper_id:zhangz15a
7 total matching documents
closed inproceedings index 

open index
Execute query: +year:[2000 TO 2000] +authors:sandro
31 total matching documents
closed inproceedings index 

open index
Execute query: +(title:fuzzy title:logic) +authors:zhang
2439 total matching documents
closed inproceedings index 

open index
Execute query: +type:article +year:[1983 TO 1983] +authors:nathan
6 total matching documents
closed inproceedings index 

open index
Execute query: +venue:acta +authors:simon
8 total matching documents
closed inproceedings index 

open index
Vocabulary dimension: 350546
closed inproceedings index 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


MainEvaluation: Main class used to compute the precision, racall, F1 scores
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
retreived docs: 10.0	 relevant docs10.0
queryNr 0	 query+title:"machine learning" +authors:zheng	 precision: 1.0	 recall: 1.0	 f1 : 1.0

retreived docs: 9.0	 relevant docs9.0
queryNr 1	 query+type:inproceedings +authors:"sanjeev saxena"	 precision: 1.0	 recall: 1.0	 f1 : 1.0

retreived docs: 2.0	 relevant docs2.0
queryNr 2	 query+type:article +title:pattern +authors:"hans ulrich simon"	 precision: 1.0	 recall: 1.0	 f1 : 1.0

retreived docs: 10.0	 relevant docs10.0
queryNr 3	 query+venue:ieee +title:"data mining" +year:[2010 TO 2010]	 precision: 1.0	 recall: 1.0	 f1 : 1.0

retreived docs: 10.0	 relevant docs10.0
	ERROR --	1	 key='journals/corr/Zhang15a	 title='On robust width property for Lasso and Dantzig selector.	 authors=[Hui Zhang]	 year=2015	 venue='CoRR	 type=article
	ERROR --	2	 key='journals/corr/ZhangZ15a	 title='Robust Visual Knowledge Transfer via EDA.	 authors=[Lei Zhang, David Zhang]	 year=2015	 venue='CoRR	 type=article
	ERROR --	3	 key='journals/ftsig/WieselZ15	 title='Structured Robust Covariance Estimation.	 authors=[Ami Wiesel, Teng Zhang]	 year=2015	 venue='Foundations and Trends in Signal Processing	 type=article
	ERROR --	4	 key='journals/ijon/ZhangZZ15a	 title='Decentralized robust attitude tracking control for spacecraft networks under unknown inertia matrices.	 authors=[Zhuo Zhang, Zexu Zhang, Hui Zhang]	 year=2015	 venue='Neurocomputing	 type=article
queryNr 4	 query+type:article +(title:robust title:model) +year:[2015 TO 2015] +authors:zhang	 precision: 0.6	 recall: 0.6	 f1 : 0.6

retreived docs: 7.0	 relevant docs7.0
queryNr 5	 query+paper_id:zhangz15a	 precision: 1.0	 recall: 1.0	 f1 : 1.0

retreived docs: 10.0	 relevant docs10.0
queryNr 6	 query+year:[2000 TO 2000] +authors:sandro	 precision: 1.0	 recall: 1.0	 f1 : 1.0

retreived docs: 10.0	 relevant docs10.0
queryNr 7	 query+(title:fuzzy title:logic) +authors:zhang	 precision: 1.0	 recall: 1.0	 f1 : 1.0

retreived docs: 6.0	 relevant docs6.0
queryNr 8	 query+type:article +year:[1983 TO 1983] +authors:nathan	 precision: 1.0	 recall: 1.0	 f1 : 1.0

retreived docs: 8.0	 relevant docs8.0
queryNr 9	 query+venue:acta +authors:simon	 precision: 1.0	 recall: 1.0	 f1 : 1.0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

IndexingFiles: Class used to create a lucene index. It specify how the fields are indexed
SearchIndex: HelperClass that execute all the query on the luceneindexes
MyPorterAnalizer: analyzer that perform: lowercase + stopword + porter stemming
MySimpleAnalizer: analyzer that perform: stopword


Parser: SAX based parser for dblp dataset
PaperInfo: class used to contains the information extracted form the Parser

MainCluster: Main class to perform the Hierarchical clustering. Based on the lingpipe library. DEPRECATED REQURE TO MUCH MEMORY




MainTopicExtraction: main class used to extract the documents to feed the BTM topic model. Also based on the SearchIndex class to get the content and the vocabulary size. The output files are saved on the correct folder for the BTMc model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
total documents for year 2000 67848
total documents for year 2001 73640
total documents for year 2002 82372
total documents for year 2003 99100
total documents for year 2004 118300
total documents for year 2005 138455
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To run the biterm topic model execute the runExample.sh script(could require chmode 777). It will automatically build and execute the c++ code.
Topic extraced are saved in BTMc/sample-data/topic_year



